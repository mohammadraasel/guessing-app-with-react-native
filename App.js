import React, { useState } from 'react'
import { SafeAreaView, StyleSheet, View, StatusBar } from 'react-native'
import Header from './components/header'
import StartGameScreen from './screens/start-game-screen'
import GameScreen from './screens/game-screen'
import GameOverScreen from './screens/game-over-screen'

const App = () => {
	const [userNumber, setUserNumber] = useState(null)
	const [guessRounds, setGuessRounds] = useState(0)

	const startGameHandler = enteredNum => {
		setUserNumber(enteredNum)
		setGuessRounds(0)
	}
	const gameOverHandler = rounds => {
		setGuessRounds(rounds)
	}
	const playAgainHandler = () => {
		setGuessRounds(0)
		setUserNumber(null)
	}

	let screen = <StartGameScreen startGame={startGameHandler} />
	if (userNumber && guessRounds <= 0) {
		screen = (
			<GameScreen userNumber={userNumber} onGameOver={gameOverHandler} />
		)
	} else if (guessRounds > 0) {
		screen = (
			<GameOverScreen
				rounds={guessRounds}
				userNumber={userNumber}
				playAgain={playAgainHandler}
			/>
		)
	}

	return (
		<>
			<StatusBar barStyle="dark-content" />
			<SafeAreaView>
				<View style={styles.screen}>
					<Header title="Guessing Nums" />
					{screen}
				</View>
			</SafeAreaView>
		</>
	)
}

const styles = StyleSheet.create({
	screen: {
		height: '100%',
	},
})

export default App
