import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 10,
		alignItems: 'center',
	},
	title: {
		fontSize: 20,
		marginVertical: 10,
	},
	card: {
		width: 300,
		maxWidth: '80%',
		alignItems: 'center',
	},
	buttonContainer: {
		flexDirection: 'row',
		width: '100%',
		justifyContent: 'space-between',
		paddingHorizontal: 15,
	},
	button: {
		width: 100,
	},
	input: {
		width: 60,
		textAlign: 'center',
	},
	summaryCard: {
		marginTop: 20,
		alignItems: 'center',
		paddingHorizontal: 50,
	},
	numberContainer: {
		alignItems: 'center',
		justifyContent: 'center',
		marginVertical: 10,
	},
	number: {
		fontSize: 35,
	},
})
