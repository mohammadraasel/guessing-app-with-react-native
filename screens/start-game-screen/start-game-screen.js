import React, { useState } from 'react'
import {
	Text,
	View,
	Button,
	TouchableWithoutFeedback,
	Keyboard,
	Alert,
} from 'react-native'
import { styles } from './styles'
import Card from '../../components/card'
import Colors from '../../constants/colors'
import Input from '../../components/input/input'
import { fonts } from '../../constants/fonts'

const StartGameScreen = props => {
	const [inputValue, setInputValue] = useState('')
	const [confirmed, setConfirmed] = useState(false)
	const [enteredNumber, setEnteredNumber] = useState(null)

	const numberInputHandler = inputText => {
		const num = inputText.replace(/[^0-9]/g, '')
		setInputValue(num)
	}
	const resetHandler = () => {
		setInputValue('')
		setConfirmed(false)
	}
	const confirmHandler = () => {
		const parsedNum = parseInt(inputValue, 10)
		if (isNaN(parsedNum) || parsedNum <= 0 || parsedNum > 99) {
			Alert.alert(
				'Invalid Number',
				'Number has to be a number between 1 and 99.',
				[{ text: 'Ok', style: 'destructive', onPress: resetHandler }],
			)
			return
		}
		setConfirmed(true)
		setEnteredNumber(parsedNum)
		setInputValue('')
		Keyboard.dismiss()
	}

	let confirmedOutput
	if (confirmed) {
		confirmedOutput = (
			<Card style={styles.summaryCard}>
				<Text style={fonts.openSansRegular}>You Entered</Text>
				<View style={styles.numberContainer}>
					<Text style={styles.number}>{enteredNumber}</Text>
				</View>
				<Button
					title="Play Now"
					color={Colors.primary}
					onPress={() => props.startGame(enteredNumber)}
				/>
			</Card>
		)
	}
	return (
		<TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
			<View style={styles.container}>
				<Text style={styles.title}>Start Game</Text>
				<Card style={styles.card}>
					<Text>Enter a number</Text>
					<Input
						blurOnSubmit
						autoCapitalize="none"
						autoCorrect={false}
						keyboardType="number-pad"
						maxLength={2}
						style={styles.input}
						value={inputValue}
						onChangeText={numberInputHandler}
					/>
					<View style={styles.buttonContainer}>
						<View style={styles.button}>
							<Button
								color={Colors.primary}
								title="Confirm"
								onPress={confirmHandler}
							/>
						</View>
						<View style={styles.button}>
							<Button
								color={Colors.error}
								title="Reset"
								onPress={resetHandler}
							/>
						</View>
					</View>
				</Card>
				{confirmedOutput}
			</View>
		</TouchableWithoutFeedback>
	)
}

export default StartGameScreen
