import { StyleSheet } from 'react-native'
import Colors from '../../constants/colors'
export const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	title: {
		fontSize: 30,
		color: Colors.error,
	},
	card: {
		width: 300,
		maxWidth: '80%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	playAgainButton: {
		marginTop: 20,
	},
	imageContainer: {
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	image: {
		width: '80%',
		height: 200,
		marginTop: 20,
		borderRadius: 10,
	},
	info: {
		marginTop: 20,
	},
})
