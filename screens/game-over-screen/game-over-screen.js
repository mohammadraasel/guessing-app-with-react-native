import React from 'react'
import { Text, View, Button, Image } from 'react-native'
import { styles } from './styles'
import Card from '../../components/card'
import { successImage } from '../../constants/images'

const GameOverScreen = ({ rounds, userNumber, playAgain }) => {
	return (
		<View style={styles.container}>
			<Card style={styles.card}>
				<Text style={styles.title}>Game is over!</Text>
				<View style={styles.info}>
					<Text>Number of rounds to win: {rounds}</Text>
					<Text>The secret number was: {userNumber}</Text>
				</View>
				<View style={styles.imageContainer}>
					<Image
						style={styles.image}
						source={successImage}
						resizeMode="cover"
					/>
				</View>
				<View style={styles.playAgainButton}>
					<Button title="Play again" onPress={playAgain} />
				</View>
			</Card>
		</View>
	)
}

export default GameOverScreen
