import React, { useState, useRef, useEffect } from 'react'
import { Text, View, Button, Alert } from 'react-native'
import { styles } from './styles'
import Card from '../../components/card'

const generateRandomNumber = (min, max, exclude) => {
	min = Math.ceil(min)
	max = Math.floor(max)
	const generatedNum = Math.floor(Math.random() * (max - min)) + min
	if (generatedNum === exclude) {
		return generateRandomNumber(min, max, exclude)
	} else {
		return generatedNum
	}
}

const Directions = {
	GREATER: 'greater',
	LOWER: 'lower',
}

const GameScreen = props => {
	const [machineGuess, setMachineGuess] = useState(
		generateRandomNumber(1, 100, props.userNumber),
	)
	const [rounds, setRounds] = useState(0)
	const currentMin = useRef(1)
	const currentMax = useRef(100)
	const { userNumber, onGameOver } = props
	useEffect(() => {
		if (machineGuess === userNumber) {
			onGameOver(rounds)
		}
	}, [machineGuess, userNumber, onGameOver, rounds])

	const nextGuessHandler = direction => {
		if (
			(direction === Directions.LOWER &&
				machineGuess < props.userNumber) ||
			(direction === Directions.GREATER &&
				machineGuess > props.userNumber)
		) {
			Alert.alert("Don't lie!", 'You know that this is wrong...', [
				{
					text: 'Sorry!',
					style: 'cancel',
				},
			])
			return
		}
		if (direction === Directions.LOWER) {
			currentMax.current = machineGuess
		} else {
			currentMin.current = machineGuess
		}
		const machineNextGuess = generateRandomNumber(
			currentMin.current,
			currentMax.current,
			machineGuess,
		)
		setMachineGuess(machineNextGuess)
		setRounds(currentRounds => currentRounds + 1)
	}
	return (
		<View style={styles.container}>
			<Text>Opponent's Guess</Text>
			<View style={styles.numberContainer}>
				<Text style={styles.number}>{machineGuess}</Text>
			</View>
			<Card style={styles.buttonContainer}>
				<View style={styles.button}>
					<Button
						title="lower"
						onPress={() => nextGuessHandler(Directions.LOWER)}
					/>
				</View>
				<View style={styles.button}>
					<Button
						title="greater"
						onPress={() => nextGuessHandler(Directions.GREATER)}
					/>
				</View>
			</Card>
		</View>
	)
}

export default GameScreen
