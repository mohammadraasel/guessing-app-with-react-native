import { StyleSheet } from 'react-native'
import Colors from '../../constants/colors'

export const styles = StyleSheet.create({
	button: {
		backgroundColor: Colors.primary,
		paddingVertical: 12,
		paddingHorizontal: 30,
	},
	buttonLabel: {
		color: 'white',
		fontFamily: 'OpenSans-Bold',
		fontSize: 18,
	},
})
