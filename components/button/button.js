import React from 'react'
import { TouchableOpacity, Text, View } from 'react-native'
import { styles } from './styles'

const Button = props => {
	return (
		<TouchableOpacity activeOpacity={0.8} onPress={props.onPress}>
			<View style={styles.button}>
				<Text style={styles.buttonLabel}>{props.label}</Text>
			</View>
		</TouchableOpacity>
	)
}

export default Button
