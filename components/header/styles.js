import { StyleSheet } from 'react-native'
import Colors from '../../constants/colors'

export const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.primary,
		width: '100%',
		height: 70,
		alignItems: 'center',
		justifyContent: 'center',
		paddingTop: 10,
	},
	title: {
		color: 'white',
		fontSize: 18,
		fontFamily: 'OpenSans-Bold',
	},
})
