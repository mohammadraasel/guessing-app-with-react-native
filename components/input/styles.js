import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
	input: {
		height: 35,
		borderBottomWidth: 1,
		borderBottomColor: 'grey',
		marginVertical: 10,
	},
})
