import { StyleSheet } from 'react-native'

export const fonts = StyleSheet.create({
	openSansBold: {
		fontFamily: 'OpenSans-Bold',
	},
	openSansRegular: {
		fontFamily: 'OpenSans-Regular',
	},
})
