export default {
	primary: '#2EA7F2',
	accent: '#F8C466',
	error: '#D9184B',
}
